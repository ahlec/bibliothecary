﻿
using System;

namespace Alexandria
{
	public interface IRequestable
	{
		Uri Url { get; }
	}
}
