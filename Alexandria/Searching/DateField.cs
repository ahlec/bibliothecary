﻿namespace Alexandria.Searching
{
	public enum DateField
	{
		Hour,
		Day,
		Week,
		Month,
		Year
	}
}
