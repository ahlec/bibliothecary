﻿namespace Alexandria.Searching
{
	public enum DateSearchCriteriaType
	{
		Exactly,
		Before,
		After,
		Between
	}
}
