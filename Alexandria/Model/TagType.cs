﻿namespace Alexandria.Model
{
	public enum TagType
	{
		Miscellaneous,
		Character,
		Relationship
	}
}
