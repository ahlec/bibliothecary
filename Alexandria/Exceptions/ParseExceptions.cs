﻿
namespace Alexandria.Exceptions
{
	public abstract class GeneralParseException
	{
	}

	public abstract class DoesNotExistException : GeneralParseException
	{
	}
}
