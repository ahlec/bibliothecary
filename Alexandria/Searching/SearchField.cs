﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alexandria.Searching
{
	public enum SearchField
	{
		BestMatch,
		Author,
		Title,
		DatedPosted,
		DateLastUpdated,
		WordCount,
		NumberLikes,
		NumberComments
	}
}
