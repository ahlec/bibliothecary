﻿
namespace Alexandria.RequestHandles
{
	// ReSharper disable once UnusedTypeParameter
	public interface IRequestHandle<T> where T : IRequestable
	{
	}
}
