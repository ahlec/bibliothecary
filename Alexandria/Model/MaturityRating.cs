﻿
namespace Alexandria.Model
{
	public enum MaturityRating
	{
		NotRated,
		General,
		Teen,
		Adult,
		Explicit
	}
}
