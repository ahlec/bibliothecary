﻿namespace Alexandria.Searching
{
	public enum NumberSearchCriteriaType
	{
		ExactMatch,
		LessThan,
		GreaterThan,
		Range
	}
}
